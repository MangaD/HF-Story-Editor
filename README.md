# HF Story Editor

Hero Fighter story editor is a program for making custom stages that can be downloaded and played by everyone. The game's stages are written in XML format, which HF can interpret.

## Developers

-   **MangaD** - Lead Developer
-   **Tommy Chan** - Developer
-   **zlyfer** - Developer

## Development Requirements

-   [NodeJS](https://nodejs.org/)
-   [Git](https://git-scm.com/downloads)
-   [Atom Editor](https://atom.io/) *(optional)*

## Naming Conventions

[JS Conventions](https://www.w3schools.com/js/js_conventions.asp)

The only exception to the rules above is that we use tabs for indentation and spaces
for alignment. Eg.

![](http://i.imgur.com/p2u0XMs.png)

**JavaScript good practices**

1.  Place `"use strict";` on top of every JavaScript file.
2.  Use `let` instead of `var` for variables.
3.  Use strict equality operator `===` and `!==` for comparisons instead of regular equality operator `==` and `!=`.
4.  Use Automated Linters (JSLint, JSHint, ESLint). ~~I also use `linter-gjslint` on Atom editor.~~ I use `linter-eslint` because it is compatible with ReactJS.
5.  Test using [Mocha](https://mochajs.org/), [Chai](http://www.chaijs.com/), [Sinon](http://sinonjs.org/). Tutorial [here](https://javascript.info/testing-mocha).
6.  Use [Babel](https://babeljs.io/) for compatibility with older browsers.

**Interesting ES6 JavaScript tricks**

*[Tutorial](http://ccoenraets.github.io/es6-tutorial/)*

1.  [Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
2.  Exponentiation operator `2 ** 3 = 2 * 2 * 2`.
3.  Unary `+` operator coverts string to number.
4.  Double NOT `!!` converts value to boolean.
5.  [Labels](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label)
6.  [Arrow functions](https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/)
7.  [Debugging](https://javascript.info/debugging-chrome)


## Atom Editor

If you're using Atom Editor (recommended) you may use the following settings and packages:

#### Settings

File » Settings » Editor »
-   Show Invisibles: on
-   Show Line Numbers: on
-   Soft Tabs: on
-   Tab Length: 4
-   Tab Type: hard

#### Packages

File » Settings » Install »
-   atom-discord
-   linter
-   linter-eslint
-   linter-markdown
-   markdown-preview
-   platformio-ide-terminal (may have problems recognizing environment variables)
-   atom-beautify
-   todo-show

## Running the project

```sh
$ npm start
```

## Building the project

1.  Comment the line `win.webContents.openDevTools(); // Debug` on the file `electron_main.js`.

2. ```sh
   $ npm run dist
   ```

## TO-DO

-   Choose installation path in electron builder.
-   [Electron Updater](https://medium.freecodecamp.org/quick-painless-automatic-updates-in-electron-d993d5408b3a)
-   Translate English to Chinese and vice-versa so that when writing a story both versions can be provided.
- Replace mp3 files or add new ones if possible.
-   Improve UI. Give it a modern look.
    -   Use Bootsrap.
    -   Have toggle menu on the left. Menu will have following options:
        -   **Drag & Drop** page, that will let users select what they want to do and have the code generated for them. **This is very low on the priority of things to do.**
        -   **Source mode** page, that is basically Ace editor.
        -   **Tutorial** page.
        -   **About** page.
    -   Both editing modes (drag & drop and source mode) will show a page that will be about selecting which stage to edit, we will call this page the story menu. More than one stage can be edited at the same time (just go to the story menu page and select another story, changes will not be lost). The story menu will have the list of stories available for editing, showing their names (that the user can edit) and *possibly* a preview of the background (that the user can select. When clicking on a stage to edit, the editor will be shown and the user can edit his text and save it to a XML file. In the editor page the user will have the option to load the original story, in case the editor is empty (this will give him a reference on how to do things). In order to generate the executable the user must go to the story menu, which will have this option available. All the stories that have been modified by the user will be replaced in the executable.
    -   Drag and drop xml opens file.
    -   Have "Save" and "Save As" buttons.
    -   Upon exit, if the file has not been saved the program should ask if the user wishes to save it.
-   Add mp3 files in the swf.
-   Add sfx when yaga waves his sword at the sample story and list the available sfx sound files somewhere.
-   Add item to story sample.
-   Make a function in utils.js for writting errors to a file. Instead of having just console.log (which users won't see), we will also write to an error.log file.
-   XLS for validating the story XML file.

## Updating

-   Check for newer versions of Ace Editor and JPEXS and read the notes below about them.

## Notes

### Ace Editor

[Ace Editor](https://ace.c9.io/) is just the best web editor for editing source code. It features syntax highlighting, themes, and a bunch of other very useful features.

However, it comes with more than we need. Ace has "modes" that we can set so that it knows which programming language we're dealing with, and switch the syntax highlighting accordingly. We only need the XML mode, so we can delete any files starting with `mode-` except for `mode-xml.js`. Similarly, we can delete all the files inside the `snippets` folder except for `xml.js`.

The editor also has themes that do not highlight the XML tags. We can remove these themes.

-   ambiance
-   dracula
-   github
-   gob
-   gruvbox
-   katzenmilch

### Editing the SWF

We use the [JPEXS Free Flash Decompiler](https://www.free-decompiler.com/flash/) program in order to replace the story XML files in the HF SWF file. This program has both a graphical UI and a command line version. We invoke the command line version programmatically so that the details on how to edit the SWF file are not obvious to the end-user.

Moreover, we only need the following files for it to work for our needs:

-   ffdec.jar
-   lib/ffdec_lib.jar
-   lib/flashdebugger.jar
-   lib/jna-x.x.x.jar

**All the other files can and should be removed.** Not only this reduces the size of our program as it will make it not possible to access the GUI of JPEXS.


### SWF 2 EXE

Finding how to programmatically convert a .swf file to a .exe file was not straightforward. There are a few programs out there that do this (e.g. [swf2exe](https://sourceforge.net/projects/swf2exe/)), but they are apparently not open source. It was by chance that I was able to find [this](https://github.com/devil-tamachan/tama-ieeye/blob/master/SWF2EXE/swf2exe/swf2exe/MainDlg.h#L98) piece of code by [devil-tamachan](https://github.com/devil-tamachan) on GitHub that showed me how to do this programmatically.

The algorithm is quite simple to implement, but I don't know yet why it works like this. For starters, there is a software called [Adobe Flash Player projector](https://www.adobe.com/support/flashplayer/debug_downloads.html), which is basically a program (.exe) that can run flash files (.swf). This program also has an option to create an .exe of the .swf file.

To convert programmatically, we do the following:

1.  Create a copy of the Flash Player projector.
2.  Append the .swf file to the projector's copy (merging the binaries basically).
3.  Append the following footer to the file: \[0x56, 0x34, 0x12, 0xFA]
4.  Append the size of the .swf file (4 bytes) to the file.

There are different versions of the Flash Player projector, but this algorithm should work for all of them.

~ MangaD

## Credits

Hero Fighter is property of Marti Wong. The HF Story Editor was developed by MangaD, Tommy Chan and zlyfer.
