const {
	app,
	BrowserWindow
} = require('electron');

const path = require('path');

function createWindow() {
	win = new BrowserWindow({
		titleBarStyle: 'hidden',
		width: 900,
		height: 650,
		icon: path.join(__dirname, '/favicon/android-chrome-192x192.png')
	});
	win.loadFile('index.html');
	//win.webContents.openDevTools(); // Debug
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});

app.on('browser-window-created', (e,window) => {
	window.setMenu(null);
});
