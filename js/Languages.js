const glob = require( 'glob' );
const path = require( 'path' );
const $ = require('jquery');

class Languages {

	constructor() {
		this._lang_arr = [];
		this._lang = null;
	}

	loadLanguages() {
		if (this._lang_arr.length === 0) {
			const that = this;
			glob.sync( './languages/*.json' ).forEach( function( file ) {
				that._lang_arr.push( require(path.resolve( file )) );
			});
		}
	}

	listLanguages() {
		let i;
		let len = this._lang_arr.length;
		for (i = 0; i < len; i++) {
			let separator = ($("#lang_list").html() ? " | " : "");
			let language = `<span id="setlang" onclick="window.lang_obj.setLanguage('${this._lang_arr[i].language}');">
					${this._lang_arr[i].language}
				</span>`;
			$("#lang_list").html($("#lang_list").html() + separator + language);
		}
	}

	get lang_arr() {
		if (this._lang_arr.length === 0) {
			return undefined;
		}
		return this._lang_arr;
	}

	get lang() {
		return this._lang;
	}

	setLanguage(l) {

		let i;
		let len = this._lang_arr.length;
		for (i = 0; i < len; i++) {
			if (this._lang_arr[i].language === l) {
				break;
			}
		}
		if (i === len) return;

		this._lang = this._lang_arr[i];
		window.lang = this._lang;

		$("title").text(this._lang.title);
		$("#title").text(this._lang.title);
		$("#save_as").val(this._lang.save_as);
		$("#generate_exe").val(this._lang.generate_exe);
		$("#replace_story_text").text(this._lang.replace_story);
		$("#xmlopen").text(this._lang.open_xml);
		$("#fontsize_text").text(this._lang.fontsize);
		$("#theme_text").text(this._lang.theme);
		$("#storyname_eng_text").text(this._lang.storyname_eng);
		$("#storyname_chi_text").text(this._lang.storyname_chi);
		$("#engtochi_text").val(this._lang.engtochi);
		$("#editor_ln").text(this._lang.editor_line);
		$("#editor_ch").text(this._lang.editor_ch);
		$("#editor_ttln").text(this._lang.editor_totalline);
		$("#replace_story_photo").text(this._lang.replace_story_photo);
		$("#loading_text").text(this._lang.loading_text);

	}
}

module.exports = Languages;
