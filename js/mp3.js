"use strict";


class formp3 {

	constructor() {
		this.file_mp3 = '';
	}

	openmp3() {

		let file_tmp = this.file_mp3;
		this.file_mp3 = $("#mp3Input").prop('files')[0];

		if (this.file_mp3.name.substring(this.file_mp3.name.lastIndexOf(".")) != ".mp3") {
			this.file_mp3 = file_tmp;
			$("#mp3Input").val("");
			popupS.alert({title: window.lang.error, content: "mp3 only"});//TODO lang
			return false;
		}

		$('#mp3file').html(this.file_mp3.name);

		return true;
	}

	static getmp3FromFile() {
		let reader = new FileReader();

		reader.onload = function() {
			let mp3 = reader.result;
			$('#mp3file').html(mp3);

		};
	}
}
