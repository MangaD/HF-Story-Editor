"use strict";

class Editor {

	constructor(ace) {
		//Get editor instance
		this.editor = ace.edit("XMLCode");

		// Set font size
		this.changeFontSize();

		// Set line height
		this.setLineHeight(1.5);

		// Set theme
		let theme = $("#theme_selector").val();
		if(!theme) {
			this.changeTheme("sqlserver");
		}

		// Set lines
		this.setLines();

		// Set mode
		this.editor.getSession().setMode("ace/mode/xml");

		// Set commads
		this.editor.commands.addCommand({
			name: 'SaveAs',
			bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
			exec: function(editor) {
				File.saveFile();
			},
			readOnly: true // false if this command should not apply in readOnly mode
		});

		/**
		 * Events
		 */
		//const that = this; // I was using an alias to this because the this
		// inside the function event was not the same this outside the function
		// event. But arrow functions don't have a this, so it takes the outside
		// one.
		this.editor.getSession().on('change', (e) => {
			this.setLines();
		});
		this.editor.getSession().selection.on('changeCursor', (e) => {
			this.setLines();
		});
		$('#fontsize_selector').on('change', (e) => {
			this.changeFontSize();
		});
		$('#theme_selector').on('change', (e) => {
			this.changeTheme();
		});
		$('#find').on('click', (e) => {
			this.execFind();
		});
		$('#findreplace').on('click', (e) => {
			this.execFindReplace();
		});
	}

	setLines() {
		let lines = this.editor.session.getLength();
		let linecol = this.editor.selection.getCursor();
		$('#lines_ln').html(linecol.row+1);
		$('#lines_ch').html(linecol.column);
		$('#lines_total').html(lines);
	}

	changeTheme(theme) {

		if (!theme) {
			theme = $("#theme_selector").val();
		}

		if (theme) {
			let fileref = document.createElement('script');
			fileref.setAttribute("type", "text/javascript");
			fileref.setAttribute("src", "ace/theme-" + theme + ".js");

			if (typeof fileref != "undefined") {
				$("head").append(fileref);
			}

			this.editor.setTheme("ace/theme/" + theme); //Set theme
		}
	}

	changeFontSize(size) {
		let s = (size ? size : $("#fontsize_selector").val());
		this.editor.setOptions({ fontSize: s });
	}

	setLineHeight(lineheight) {
		let ln = (lineheight ? lineheight : 1.5);
		this.editor.container.style.lineHeight = ln;
		this.editor.renderer.updateFontSize();
	}

	execFind() {
		this.editor.execCommand("find");
	}

	execFindReplace() {
		this.editor.execCommand("replace");
	}

}
