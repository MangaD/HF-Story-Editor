"use strict";

const $ = require('jquery');
const popupS = require('popups');
const Languages = require('./js/Languages.js');

$(document).ready(function() {

	// Check for API support
	File.fileSupport();

	// Set language
	window.lang_obj = new Languages();
	window.lang_obj.loadLanguages();
	window.lang_obj.listLanguages();
	window.lang_obj.setLanguage("English");

	// Initialize editor
	let editor = new Editor(ace);

	// Events
	$('#fileInput').change(function(e) {
		if (File.openFile()) {
			File.getDataFromFile();
		}
		$(this).val(''); // Sets the value to '' so the 'change' event is triggered even if the same file is opened again. Might be problematic if val needs to be used later again.
	});
	$('#save_as').click((e) => {
		File.saveFile();
	});
	$('#generate_exe').click((e) => {
		if (File.getDataFromEditor()) {
			generateExe();
		}
	});
	$('#engtochi_text').click((e) => {
		engtochi();
	});
	/*
	$('#mp3Input').change((e) => {
		if (formp3.openmp3()) {
			formp3.getmp3FromFile();
		}
	});
	$('#mp3debug').click((e) => {
		console.log($("#mp3Input").val());
	});*/

});
