"use strict";


// Access File System
const fs = require('fs');


let file;
let saved = false;


class File {

	static fileSupport() {
		// Check for the various File API support.
		if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
			popupS.alert({title: window.lang.error, content: window.lang.api_error});
		}
	}

	static getDataFromEditor() {

		const data = ace.edit("XMLCode").getValue();

		if (!data) {
			popupS.alert({title: window.lang.error, content: window.lang.empty_textarea_error});
			return null;
		}
		return data;
	}

	static saveFile() {

		const data = this.getDataFromEditor();

		if (data !== null && saved) {
			//save
			let fileName = document.getElementById("saveName").value;
			fs.writeFile(fileName, data, function(err) {
				if (err) {
					popupS.alert({title: window.lang.error, content: err});
				}
				else {
					popupS.alert({content: window.lang.saved});
				}
			});

		} else if (data !== null) {
			//Save-as
			let fileName = "new.xml";

			if (file && file.name) {
				fileName = file.name;
			}

			require("electron").remote.dialog.showSaveDialog({
				title: window.lang.prompt_text,
				defaultPath: `~/${fileName}`
			}, function(result) {

				fileName = result;

				if (!fileName) {
					return;
				}

				fs.writeFile(fileName, data, function(err) {
					if (err) {
						popupS.alert({title: window.lang.error, content: err});
					}
					else {
						popupS.alert({content: window.lang.saved});
					}
				});
				document.getElementById("saveName").value = fileName;
				saved = true;
			});
		}
	}

	static openFile() {

		let file_tmp = file;
		file = $("#fileInput").prop('files')[0];
		let fileName = escape(file.name);
		let fileType = (file.type ? file.type : 'n/a');
		let fileSize = file.size;
		let lastModified = file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a';

		if (file.name.substring(file.name.lastIndexOf(".")) != ".xml") {
			file = file_tmp;
			$("#fileInput").val("");
			popupS.alert({title: window.lang.error, content: window.lang.xml_only_error});
			return false;
		} else {
			fileType = "text/xml";
		}

		document.getElementById('fileDetails').innerHTML =
			'<strong>' + fileName + '</strong> (' + fileType + ') - ' +
			fileSize + window.lang.editor_size_lastm + lastModified;

		return true;
	}

	static getDataFromFile() {
		let reader = new FileReader();

		reader.onload = function() {
			let data = reader.result;
			let editor = ace.edit("XMLCode");
			editor.setValue(data, -1);
			editor.getSession().setUndoManager(new ace.UndoManager());

			try {
				let bgid = data.match(/<bgid>(.*)<\/bgid>/i)[1];
				if (bgid) {
					let exists = 0 != $('#bg_selector option[value='+bgid+']').length;
					if (exists) {
						$("#bg_selector").val(bgid);
					}
				}
			} catch(err) {}
		};

		reader.readAsText(file);
	}

}
