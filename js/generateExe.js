"use strict";

// Access File System
//const fs = require("fs");
const path = require('path');

// Execute external program
const {	spawn } = require('child_process');

const translate = require('google-translate-api');

let javaExe = null;

let xmlData;
let xml_story_filename;
let xml_storylist_filename;
let swf_filename;
let exe_filename;
let story_name;
let story_nameb5;
let story_number;
let bg_id;

function engtochi() {
	let eng = $("#storyname");
	let chi = $("#storynameb5");
	translate(eng.val(), {from: 'en', to: 'zh-TW'}).then(res => {
		chi.val(res.text);
	}).catch(err => {
		popupS.alert({title: window.lang.error, content: err});
	});
}

function generateExe() {

	try {
		xmlData = File.getDataFromEditor();
	} catch (err) {
		console.log(err);
		popupS.alert({title: window.lang.error, content: err});
		return;
	}

	if (xmlData === undefined || xmlData === null || xmlData === '' || xmlData.length === 0) {
		popupS.alert({title: window.lang.error, content: window.lang.empty_story_error});
		return;
	}

	if (xmlData.length > 512 * 1000) {
		popupS.alert({title: window.lang.error, content: window.lang.large_size_error});
		return;
	}

	story_name = $("#storyname").val();
	story_nameb5 = $("#storynameb5").val();
	story_number = $("#storynum").val() - 1;
	bg_id = $("#bg_selector").val();

	if (story_number > 6 || story_number < 0) {
		popupS.alert({title: window.lang.error, content: window.lang.replace_story_error});
		return;
	}

	if (story_name === "" || story_name === null) {
		popupS.alert({title: window.lang.error, content: window.lang.story_engname_error});
		return;
	}

	if (story_nameb5 === "" || story_nameb5 === null) {
		popupS.alert({title: window.lang.error, content: window.lang.story_chiname_error});
		return;
	}

	$('body').addClass("loading");
	if (javaExe !== null) {
		generateExe2();
	}
	else {
		let isWin = process.platform === "win32";

		let spawn = require('child_process').spawnSync('java', ['-version']);
		let data = spawn.stderr + spawn.stdout;
		data = data.toString().split('\n')[0] ;
		let version = new RegExp('version').test(data) ? data.split(' ')[2].replace(/"/g, '') : false;
		if (version !== false) {
			javaExe = 'java';
			generateExe2();
		} else {
			if (isWin) {
				let version_w;
				let key = 'HKLM\\SOFTWARE\\JavaSoft\\Java Runtime Environment';
				// Electron builder can't access this file if it is inside app.asar,
				// So we need to add "node_modules/regedit/vbs/*" to "extraFiles" in
				// package.json and set the path here
				require('regedit').setExternalVBSLocation('./node_modules/regedit/vbs/');
				require('regedit').list(key,
					function(err, result) {
						if (err) {
							key = 'HKLM\\SOFTWARE\\Wow6432Node\\JavaSoft\\Java Runtime Environment';
							require('regedit').list(key,
								function(err, result) {
									if (err) {
										$('body').removeClass("loading");
										popupS.alert({title: window.lang.error, content: window.lang.java_error});
										console.log(err);
										return;
									}
									version_w = result[key]['values']['CurrentVersion']['value'];
									key = key + '\\' + version_w;
									require('regedit').list(key, function(err, result) {
										if (err) {
											$('body').removeClass("loading");
											popupS.alert({title: window.lang.error, content: window.lang.java_error});
											console.log(err);
											return;
										}
										javaExe = result[key]['values']['JavaHome']['value'] + '\\bin\\java.exe';
										generateExe2();
									});
								});
							return;
						}
						version_w = result[key]['values']['CurrentVersion']['value'];
						key = key + '\\' + version_w;
						require('regedit').list(key, function(err, result) {
							if (err) {
								$('body').removeClass("loading");
								popupS.alert({title: window.lang.error, content: window.lang.java_error});
								console.log(err);
								return;
							}
							javaExe = result[key]['values']['JavaHome']['value'] + '\\bin\\java.exe';
							generateExe2();
						});
				});
			} else {
				$('body').removeClass("loading");
				popupS.alert({title: window.lang.error, content: window.lang.java_error});
			}
		}
	}
}

function generateExe2() {

	// Generate Date String to use in default file name
	let date = new Date();
	let year = date.getFullYear().toString();
	let month = date.getDate().toString();
	let day = date.getDay().toString();
	let hour = date.getHours().toString();
	let minute = date.getMinutes().toString();
	let second = date.getSeconds().toString();
	let millis = date.getMilliseconds().toString();
	let default_filename = `${year}-${month}-${day}-${hour}-${minute}-${second}-${millis}`;

	xml_story_filename = default_filename + '_story.xml';
	xml_storylist_filename = default_filename + '_storylist.xml';
	swf_filename = default_filename + '.swf';
	exe_filename = default_filename + '.exe';

	let storyListXML = generateStoryList(story_name, story_nameb5, bg_id);

	// TODO: Story XML validation with XLS


	require("electron").remote.dialog.showSaveDialog({
		title: window.lang.prompt_text,
		defaultPath: exe_filename
	}, function(result) {

		if(!result) {
			$('body').removeClass("loading");
			return;
		}

		exe_filename = result;

		let dir = path.dirname(exe_filename);

		xml_story_filename = path.join(dir, xml_story_filename);
		xml_storylist_filename = path.join(dir, xml_storylist_filename);
		swf_filename = path.join(dir, swf_filename);

		// MangaD note: Functions should be asynchronous because otherwise the loading animation will block

		// Write Story XML file
		fs.writeFile(xml_story_filename, xmlData, function(err) {
			if (err) {
				$('body').removeClass("loading");
				console.log(err);
				popupS.alert({title: window.lang.error, content: window.lang.writexml_error});
				return;
			}

			// Write StoryList XML file
			fs.writeFile(xml_storylist_filename, storyListXML, function(err) {
				if (err) {
					fs.unlink(xml_story_filename, function(error) {
						if (error) {
							console.log(err);
							popupS.alert({title: window.lang.error, content: window.lang.writexml_error});
						}
					});
					$('body').removeClass("loading");
					console.log(err);
					popupS.alert({title: window.lang.error, content: window.lang.writexml_error});
					return;
				}


				// Create EXE file
				generateExe3();
			});
		});

	});
}

function generateExe3() {

	let story_ids = [
		"219",
		"398",
		"226",
		"299",
		"305",
		"454",
		"170"
	]
	let storylist_id = "234";

	// NOTE: Functions should be asynchronous because otherwise the loading animation will block

	// Replace Story XML
	let ffdec = spawn(javaExe, ['-jar', 'ffdec/ffdec.jar', '-replace',
		'game/hf.swf', swf_filename, story_ids[story_number], xml_story_filename]);

	ffdec.stdout.on('data', (data) => {
		console.log(`stdout: ${data}`);
	});

	ffdec.stderr.on('data', (data) => {
		console.log(`stderr: ${data}`);
	});

	ffdec.on('exit', (code) => {
		console.log(`child process exited with code ${code}`);
		if (code != 0) {
			popupS.alert({title: window.lang.error, content: window.lang.replace_xml});
			return;
		}

		// Replace Story List XML
		let ffdec2 = spawn(javaExe, ['-jar', 'ffdec/ffdec.jar', '-replace',
			swf_filename, swf_filename, storylist_id, xml_storylist_filename]);

		ffdec2.stdout.on('data', (data) => {
			console.log(`stdout: ${data}`);
		});

		ffdec2.stderr.on('data', (data) => {
			console.log(`stderr: ${data}`);
		});

		ffdec2.on('exit', (code) => {
			console.log(`child process exited with code ${code}`);
			if (code != 0) {
				popupS.alert({title: window.lang.error, content: window.lang.replace_xml});
				return;
			}
			/*
			let mp3file = document.getElementById("mp3file").innerHTML;
			if (mp3file != "" && mp3file != null) {
				//replace mp3
				let remp3file = document.getElementById("mp3_selector").value;
				let ffdec4 = spawn(javaExe, ['-jar', 'ffdec/ffdec.jar', '-replace',
					swf_filename, swf_filename, remp3file, mp3file]);

				ffdec4.stdout.on('data', (data) => {
					console.log(`stdout: ${data}`);
				});

				ffdec4.stderr.on('data', (data) => {
					console.log(`stderr: ${data}`);
				});

				ffdec4.on('exit', (code) => {
					console.log(`child process exited with code ${code}`);
					if (code != 0) {
						$('body').removeClass("loading");
						popupS.alert({title: window.lang.error, content: window.lang.replace_xml});
						return;
					}
				});
			}
			*/


			// Compress SWF
			let ffdec3 = spawn(javaExe, ['-jar', 'ffdec/ffdec.jar',
				'-compress', 'zlib', swf_filename, swf_filename + "_compressed"]);

			ffdec3.stdout.on('data', (data) => {
				console.log(`stdout: ${data}`);
			});

			ffdec3.stderr.on('data', (data) => {
				console.log(`stderr: ${data}`);
			});

			ffdec3.on('exit', (code) => {
				console.log(`child process exited with code ${code}`);
				if (code != 0) {
					$('body').removeClass("loading");
					popupS.alert({title: window.lang.error, content: window.lang.replace_xml});
					return;
				}

				try {
					fs.unlinkSync(swf_filename);
				} catch(err) {
					$('body').removeClass("loading");
					console.log(err);
					popupS.alert({title: window.lang.error, content: window.lang.del_swf_error});
					return;
				}

				swf_filename = swf_filename + "_compressed";

				swf2exe(swf_filename, exe_filename);

			});
		});
	});


}

function swf2exe(swfFile, outputFile) {

	let projectorData = fs.readFileSync(`swf2exe/SA.exe`);
	let swfData = fs.readFileSync(swfFile);
	let footer = Buffer.from( new Uint8Array([0x56, 0x34, 0x12, 0xFA]) );
	let swfLength = Buffer.from( new Uint8Array(intToByteArray(swfData.length)));

	// Windows was having an error "EBUSY resource busy or locked" when
	// using appendFile after writeFile, so we just write everything at once
	let arr = [projectorData, swfData, footer, swfLength];
	let buf = Buffer.concat(arr);

	fs.writeFile(outputFile, buf, function (err) {

		if (err) {
			$('body').removeClass("loading");
			popupS.alert({title: window.lang.error, content: window.lang.swf2exe_error});
			return console.log(err);
		}
		// Delete XML and SWF files
		try {
			fs.unlinkSync(xml_story_filename);
			fs.unlinkSync(xml_storylist_filename);
			fs.unlinkSync(swf_filename);
		} catch(err) {
			$('body').removeClass("loading");
			console.log(err);
			popupS.alert({title: window.lang.error, content: window.lang.unlink_error});
			return;
		}

		popupS.alert({title: window.lang.success, content: window.lang.exe_success});

		$('body').removeClass("loading");

	});

	function intToByteArray(int) {
		// we want to represent the input as a 4-bytes array
		var byteArray = [0, 0, 0, 0];

		for ( var index = 0; index < byteArray.length; index ++ ) {
			var byte = int & 0xff;
			byteArray [ index ] = byte;
			int = (int - byte) / 256 ;
		}

		return byteArray;
	};
}


function generateStoryList(story_name, story_nameb5, bg_id) {

	let story_list = [
		"First Match",
		"Trap",
		"Defeated",
		"Break in",
		"Prison break",
		"Waken",
		"Promises",
		"Training",
		"Betrayal",
		"Death Match"
	];

	let story_listb5 = [
		"初戰",
		"陷阱",
		"潰敗",
		"潛入",
		"逃獄",
		"醒覺",
		"約定",
		"特訓",
		"背叛",
		"死戰"
	];

	let bg_ids = [
		"story01",
		"story02b",
		"story03a",
		"story04a",
		"prison2",
		"story06c",
		"desert",
		"hill_grass",
		"forest_grass",
		"room1"
	];

	story_list[story_number] = story_name;
	story_listb5[story_number] = story_nameb5;
	bg_ids[story_number] = (bg_id ? bg_id : bg_ids[story_number]);

	return `<storylist>
	<story>
		<storyid>story01</storyid>
		<bgid>${bg_ids[0]}</bgid>
		<storyname>
			<en>1. ${story_list[0]}</en>
			<b5>第一話 ${story_listb5[0]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story02</storyid>
		<bgid>${bg_ids[1]}</bgid>
		<storyname>
			<en>2. ${story_list[1]}</en>
			<b5>第二話 ${story_listb5[1]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story03</storyid>
		<bgid>${bg_ids[2]}</bgid>
		<storyname>
			<en>3. ${story_list[2]}</en>
			<b5>第三話 ${story_listb5[2]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story04</storyid>
		<bgid>${bg_ids[3]}</bgid>
		<storyname>
			<en>4. ${story_list[3]}</en>
			<b5>第四話 ${story_listb5[3]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story05</storyid>
		<bgid>${bg_ids[4]}</bgid>
		<storyname>
			<en>5. ${story_list[4]}</en>
			<b5>第五話 ${story_listb5[4]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story06</storyid>
		<bgid>${bg_ids[5]}</bgid>
		<storyname>
			<en>6. ${story_list[5]}</en>
			<b5>第六話 ${story_listb5[5]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story07</storyid>
		<bgid>${bg_ids[6]}</bgid>
		<storyname>
			<en>7. ${story_list[6]}</en>
			<b5>第七話 ${story_listb5[6]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story08</storyid>
		<bgid>${bg_ids[7]}</bgid>
		<storyname>
			<en>8. ${story_list[7]}</en>
			<b5>第八話 ${story_listb5[7]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story09</storyid>
		<bgid>${bg_ids[8]}</bgid>
		<storyname>
			<en>9. ${story_list[8]}</en>
			<b5>第九話 ${story_listb5[8]}</b5>
		</storyname>
	</story>
	<story>
		<storyid>story10</storyid>
		<bgid>${bg_ids[9]}</bgid>
		<storyname>
			<en>10. ${story_list[9]}</en>
			<b5>第十話 ${story_listb5[9]}</b5>
		</storyname>
	</story>
</storylist>
`;

}
